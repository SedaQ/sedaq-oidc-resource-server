## SedaQ OIDC Resource Server

Maven and Java SDK must be installed to build the project.

```bash
apt get install openjdk-11-jdk maven
```

## Docker

### Build and Run OIDC Resource Server
Docker build.

```bash
docker build -t sedaq-oidc-resource-server .
```


Docker run.
```bash
docker run -p 8081:8081 -it --name sedaq-resource-server-container sedaq-oidc-resource-server
```