package org.sedaq.security;

import com.google.gson.JsonObject;
import org.mitre.oauth2.introspectingfilter.service.IntrospectionAuthorityGranter;
import org.sedaq.security.enums.AuthenticatedUserOIDCItems;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Class is annotated with {@link Component}, so its mark as candidates for auto-detection when using annotation-based configuration and classpath scanning.
 * This class is responsible for returning a set of Spring Security GrantedAuthority objects to be assigned to the token service's resulting <i>Authentication</i> object.
 *
 * @author Pavel Seda
 */
@Component
public class CustomAuthorityGranter implements IntrospectionAuthorityGranter {

    private static final String USER_INFO_ENDPOINT = "/users/info";

    @Override
    public List<GrantedAuthority> getAuthorities(JsonObject introspectionResponse) {
        System.out.println(introspectionResponse);
        String login = introspectionResponse.get(AuthenticatedUserOIDCItems.SUB.getName()).getAsString();
        String iss = introspectionResponse.get(AuthenticatedUserOIDCItems.ISS.getName()).getAsString();

        // call microservice that holds the users or access the database to retrieve info about logged in user roles
        return List.of(new SimpleGrantedAuthority("ROLE_USER"));
    }

}
