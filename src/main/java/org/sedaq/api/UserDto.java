package org.sedaq.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Pavel Seda
 */
@ApiModel(value = "UserDto", description = "User basic info.")
public class UserDto {
    @ApiModelProperty(value = "Main identifier of user dto.", required = false, example = "1", position = 1)
    private Long id;
    @ApiModelProperty(value = "A given name of an user.", required = false, example = "Pavel", position = 2)
    private String givenName;
    @ApiModelProperty(value = "A family name of an user.", required = false, example = "Seda", position = 3)
    private String familyName;

    public UserDto() {
    }

    public UserDto(Long id, String givenName, String familyName) {
        this.id = id;
        this.givenName = givenName;
        this.familyName = familyName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", familyName='" + familyName + '\'' +
                ", givenName='" + givenName + '\'' +
                '}';
    }
}

