package org.sedaq.service;

import org.sedaq.api.UserDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService {

    // security annotations here
    public UserDto getUserById(Long id) {
        // call repository to get information
        return new UserDto(id, "Pavel", "Seda");
    }
}
