package org.sedaq.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.sedaq.api.UserDto;
import org.sedaq.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Pavel Seda
 */
@Api(value = "/users", tags = "Users", consumes = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(path = "/users")
public class UserRestController {

    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(httpMethod = "GET",
            value = "Get User by Id.",
            response = UserDto.class,
            nickname = "getUserById",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<UserDto> getUserById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

}
