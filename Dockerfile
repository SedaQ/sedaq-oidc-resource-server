FROM maven:3.6.2-jdk-11-slim AS plr
LABEL maintaner="Pavel Seda"
COPY ./ /apps
WORKDIR /apps
RUN mvn clean package -DskipTests

FROM openjdk:11-jdk AS jdk
COPY --from=plr /apps/target/sedaq-oidc-resource-server-*.jar /apps/sedaq-oidc-resource-server.jar
WORKDIR /apps

CMD ["java", "-jar",  "sedaq-oidc-resource-server.jar"]
EXPOSE 8081